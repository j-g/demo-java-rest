#!/bin/bash
# Copyright (c) 2021 James Gardner

# Drawing information from the Maven project configuration, outputs a series of variable initialization commands and
# their exports, which can then be source'd or eval'd to create an environment containing project information, needed
# for non-Maven build operations. This can be used to cache the information until such time as Maven (eg. POM) project
# configuration is modified, thereby eliminating slow Maven operations from subsequent iterations of those build steps.

set -ex

APP_ARTIFACT_ID=$(mvn -q org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=project.artifactId -DforceStdout)
printf "export APP_ARTIFACT_ID=%q\n" "$APP_ARTIFACT_ID"

APP_VERSION=$(mvn -q org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=project.version -DforceStdout)
printf "export APP_VERSION=%q\n" "$APP_VERSION"

APP_JAR_NAME=$(mvn -q org.apache.maven.plugins:maven-help-plugin:3.2.0:evaluate -Dexpression=project.build.finalName -DforceStdout)
printf "export APP_JAR_NAME=%q\n" "$APP_JAR_NAME"

APP_MODULE_PATHS_TEMP_FILE_PATH=$(mktemp)
mvn -q dependency:build-classpath -DincludeScope=runtime -Dmdep.pathSeparator=$'\n' -Dmdep.outputFile="$APP_MODULE_PATHS_TEMP_FILE_PATH"
APP_NEEDED_MODULE_PATHS="target/${APP_JAR_NAME}.jar"$'\n'"$(cat "$APP_MODULE_PATHS_TEMP_FILE_PATH")"
rm "$APP_MODULE_PATHS_TEMP_FILE_PATH"
printf "export APP_NEEDED_MODULE_PATHS_LINE=%q\n" "$(echo -n "$APP_NEEDED_MODULE_PATHS" | paste -s -d ":")"

printf "export APP_IMAGE_RELEASE_TAG=%q\n" "registry.gitlab.com/j-g/demo-java-rest/$(echo -n "${APP_ARTIFACT_ID}:${APP_VERSION}" | tr '[:upper:]' '[:lower:]')"
