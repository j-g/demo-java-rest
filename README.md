*Copyright (c) 2021 James Gardner*

# Demo - Java Rest

## What It Is

A web/micro services app deliverable as a runnable, self-contained Docker image, created solely to demonstrate usage of
various Java web-related libraries, frameworks and code, in the implementation of a RESTful, JSON-based API.

## Where It Is

The project home is at: <https://gitlab.com/j-g/demo-java-rest> .

This project is deployed for viewing/use at: <https://demojavarest.james.j-servers.net> .

The very same runtime Docker image is available from: <https://gitlab.com/j-g/demo-java-rest/container_registry/> .

## How To Build And Run It

This project not only produces a Docker runtime image, but also uses Docker (and Docker Compose) for building (it).

Therefore, to build and run this app on your local machine, you need *only* Docker and Docker Compose installed.
(Java, Maven, etc. are not required since they are present in the Docker images used for building and/or running the app.) 

NOTE: The aforementioned build process has only been tested on a Linux-based Docker host.

To build the app and run it:

- Clone the Git repository to an empty directory on your local machine.
- **Make that target directory your current directory.**
- Type `./devcycle`, which will run the build scripts, and the resulting app in a Docker container.
    
    (NOTE: The first time you run this on your local machine, expect a flurry of activity, as required Docker images and
    Maven artifacts are downloaded, and project files built. Subsequent builds should be dramatically quicker once most
    things are cached.)
- Once the build is complete, you should start seeing (mostly Spring) logging output from the running app. Once that stops,
  the app should then be available for use.
- Then, in a separate shell, run `sudo docker container ls` to display a list of running containers. You will notice
  that running this app has created two:
    - One is named `demojavarest_volbrowser_1` and is a small, convenience web server for providing visibility into the
        contents of the named Docker volumes that the build system uses for caching Maven artifacts and compiled project
        files. That same line should tell you which port on your host it is using/mapped to, and should look like
        `127.0.0.1:8889->8888` . In that example, the host port is `8889`, so you can use it by browsing to: 
        `http://localhost:8889` .
    - The other is named similarly to `demojavarest_runner_run_XXXXXXXXXXXX`, and is the actual demo web app container.
        Again, that line will tell you which host port it is listening on, so you know to browse to
        `http://localhost:<that port>`. The port will increase each time the app is run; it is mapped to a 'random' port
        in that fashion so as to avoid conflicts with other servers the developer may have running on their local machine.

### How To Clean Up After Local Builds/Use

When you are finished with the app locally, you may wish to clean up local resources created/downloaded during its
use/building. While still in the project directory, you can:

- Press `Ctrl-C` in the terminal in which you originally ran `./devcycle`, to stop the running app. 
- Run `sudo docker-compose down` to stop the `demojavarest_volbrowser_1` helper container.
- Run `sudo docker image rm <name of image>...` to remove images, where the names of images to remove will be similar to: 
    - `demo-java-rest`
    - `registry.gitlab.com/j-g/demo-java-rest/demo-java-rest:1.0.0-snapshot` (Change `1.0.0-snapshot` to the version of
      the app you were running.)
    - `registry.gitlab.com/j-g/utility-docker-image-builds/jdk15_mvn3_adds_multiplatform_jmods_jdk:jdk-15.0.1`
      (or a similar version).
    - `demo-java-rest-builder`
    - `adoptopenjdk:15-jre-hotspot-focal` (or similar, assuming you are not using that image for another project!).
    - `python:3.9-alpine` (or similar, assuming you are not using that image for another project!).
- Prune untagged images from the local cache, by running `sudo docker image prune` .
- The app building process creates named Docker volumes which you can delete using
  `sudo docker volume rm <name of volume>...`, where the volume names are:
    - `demojavarest_build-m2`
    - `demojavarest_build-runtimes`
    - `demojavarest_build-target`

## If You Experience A Problem Or See A Bug

If you wish, you can create an issue [here](https://gitlab.com/j-g/demo-java-rest/issues).

## Disclaimer

While the demo software should not cause any problems on your system, or make
changes to it beyond those needed for running the demo (eg. possible creation of
temporary files), please note that permission to use the software is granted to
you only upon your agreement with the terms stated in the file named `LICENSE`
which accompanies the software, and in particular your attention is drawn to the
following paragraph within:

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
