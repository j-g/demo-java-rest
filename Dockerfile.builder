# Copyright (c) 2021 James Gardner

FROM registry.gitlab.com/j-g/utility-docker-image-builds/jdk15_mvn3_adds_multiplatform_jmods_jdk:jdk-15.0.1

VOLUME /app/runtimes/ /app/target/ /root/.m2/

ENV PATH="${PATH}:/app"

WORKDIR /app

COPY build_* buildsteps Dockerfile.runtime* pom.xml ./
COPY src/ src/
