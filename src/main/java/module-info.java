// Copyright (c) 2021 James Gardner

module net.j_servers.james.demo_java_rest {

    requires io.swagger.v3.oas.annotations;
    requires io.swagger.v3.oas.models;

    requires java.persistence;

    requires org.slf4j;

    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
    requires spring.context;
    requires spring.core;
    requires spring.data.commons;
    requires spring.data.rest.core;
    requires spring.data.rest.webmvc;
    requires spring.web;
    requires spring.webmvc;

}
