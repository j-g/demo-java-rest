
package net.j_servers.james.demo_java_rest.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;


@Entity
public class PhoneNumber {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String number;
    private Type type;


    public PhoneNumber() {
    }

    public PhoneNumber(Long id, String number, Type type) {
        this.id = id;
        this.number = number;
        this.type = type;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ( !(obj instanceof PhoneNumber) ) {
            return false;
        }
        PhoneNumber phoneNumber = (PhoneNumber)obj;
        return Objects.equals(id, phoneNumber.id)
            && Objects.equals(number, phoneNumber.number)
            && Objects.equals(type, phoneNumber.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, type);
    }

    @Override
    public String toString() {
        return "PhoneNumber{id=" + id + ", number='" + number + "', type=" + type + "}";
    }


    public enum Type {
        home,
        work,
        mobile
    }

}
