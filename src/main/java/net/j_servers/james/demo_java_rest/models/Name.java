
package net.j_servers.james.demo_java_rest.models;


import java.util.Objects;

import javax.persistence.Embeddable;


@Embeddable
public class Name {

    private String first;
    private String middle;
    private String last;


    public Name() {
    }

    public Name(String first, String middle, String last) {
        this.first = first;
        this.middle = middle;
        this.last = last;
    }


    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getMiddle() {
        return middle;
    }

    public void setMiddle(String middle) {
        this.middle = middle;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ( !(obj instanceof Name) ) {
            return false;
        }
        Name name = (Name)obj;
        return Objects.equals(first, name.first)
            && Objects.equals(middle, name.middle)
            && Objects.equals(last, name.last);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, middle, last);
    }

    @Override
    public String toString() {
        return "Name{first='" + first + "', middle='" + middle + "', last='" + last + "'}";
    }

}
