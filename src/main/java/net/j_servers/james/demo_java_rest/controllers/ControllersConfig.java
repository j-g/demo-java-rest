// Copyright (c) 2021 James Gardner

package net.j_servers.james.demo_java_rest.controllers;


import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

import net.j_servers.james.demo_java_rest.models.Contact;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;


@Configuration
public class ControllersConfig {

    @Bean
    public RepositoryRestConfigurer appRepositoryRestConfigurer(@Value("${this-app.ws.v1.services.base-path}") String basePath) {
        return RepositoryRestConfigurer.withConfig(
            (rrc, cr) -> rrc.setBasePath(basePath)
                .exposeIdsFor(Contact.class)
        );
    }

    @Bean
    public OpenAPI customOpenAPI(
        @Value("${this-app.name}") String appName,
        @Value("${this-app.version}") String appVersion,
        @Value("${this-app.description}") String appDescription
    ) {
        return new OpenAPI()
            .info(
                new Info()
                    .title(appName)
                    .version(appVersion)
                    .description(appDescription)
            );
    }

}