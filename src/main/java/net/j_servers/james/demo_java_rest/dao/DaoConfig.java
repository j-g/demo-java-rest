// Copyright (c) 2021 James Gardner

package net.j_servers.james.demo_java_rest.dao;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;


@Configuration
public class DaoConfig {

    private static final Logger log = LoggerFactory.getLogger(DaoConfig.class);


    @Bean
    public Jackson2RepositoryPopulatorFactoryBean respositoryPopulator(@Value("${this-app.db.default-records-filename}") String defaultDbRecordsFilename) {
        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
        factory.setResources(
            new Resource[] { new ClassPathResource(defaultDbRecordsFilename) }
        );
        return factory;
    }

}
