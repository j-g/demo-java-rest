// Copyright (c) 2021 James Gardner

package net.j_servers.james.demo_java_rest.dao;


import io.swagger.v3.oas.annotations.tags.Tag;

import net.j_servers.james.demo_java_rest.models.Contact;

import org.springframework.data.repository.PagingAndSortingRepository;


@Tag(name = "API v1")
public interface ContactRepository extends PagingAndSortingRepository<Contact, Long> {
}
